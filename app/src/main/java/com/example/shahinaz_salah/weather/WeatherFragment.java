package com.example.shahinaz_salah.weather;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shahinaz_salah.weather.adaptors.OnRecyclerViewItemClickListener;
import com.example.shahinaz_salah.weather.adaptors.WeatherAdaptor;
import com.example.shahinaz_salah.weather.database.WeatherDB;
import com.example.shahinaz_salah.weather.entities.City;
import com.example.shahinaz_salah.weather.entities.Weather;
import com.example.shahinaz_salah.weather.entities.WeatherList;
import com.example.shahinaz_salah.weather.services.ClientRespomseIF;
import com.example.shahinaz_salah.weather.services.GetWeatherService;
import com.example.shahinaz_salah.weather.utility.ConnectivityReceiverListener;
import com.example.shahinaz_salah.weather.utility.Constant;
import com.example.shahinaz_salah.weather.utility.NetworkChangeReceiver;
import com.example.shahinaz_salah.weather.utility.SharedPreference;
import com.squareup.picasso.Picasso;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class WeatherFragment extends Fragment  implements OnRecyclerViewItemClickListener, ConnectivityReceiverListener {


    @BindView(R.id.todayTempTxt)
    TextView todayTemp;

    @BindView(R.id.todayDescrTxt)
    TextView todayDesc;

    @BindView(R.id.todayHumidityTxt)
    TextView todayHumidity;

    @BindView(R.id.todayPressureTxt)
    TextView todayPressure;

    @BindView(R.id.todayMinTxt)
    TextView todayTempMin;

    @BindView(R.id.todayMaxTxt)
    TextView todayTempMax;

    @BindView(R.id.todayWindTxt)
    TextView todayWind;

    @BindView(R.id.todayIcon)
    ImageView todayIcon;

    @BindView(R.id.lastUpdate)
    TextView lastUpdateTxt;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.spinner)
    SearchableSpinner spinner;

    @BindView(R.id.horizontal_recycle_items)
    RecyclerView horizontalRecyclerView;

    private WeatherList weather;
    private ArrayList<City> cityData;
    private SharedPreference sp;
    private int selectCityId;
    private ArrayList<Weather> fiveDaysWeather;
    private WeatherAdaptor adp;
    private WeatherDB db;
    private NetworkChangeReceiver receiver;
    private Boolean isOffline = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)  {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        ButterKnife.bind(this, view);
        receiver = new NetworkChangeReceiver();
        receiver.addListener(this);
        getContext().registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        hideProgressBar();
        sp = new SharedPreference(getContext());
        db = new WeatherDB(getContext());
        loadCitySpinner();
        return view;
    }

    public void getWeatherData(){
        GetWeatherService service = new GetWeatherService(getContext(), selectCityId, new ClientRespomseIF() {
            @Override
            public void OnResponseReceived(Object result) {
                hideProgressBar();
                weather = (WeatherList)result;
                if(weather != null){
                    saveDataInDatabase(weather);
                    fillWeatherData(weather);
                }else {
                    Toast.makeText(getContext(), R.string.toast_getweather_error, Toast.LENGTH_SHORT).show();
                }
            }
        });
        service.execute();
        showProgressBar();
    }


    public void fillWeatherData(WeatherList weather){
        try{
            Weather weathe = weather.getWeather().get(0);
            todayTemp.setText(weathe.getTemp().toString() + getResources().getString(R.string.pref_units_string));
            todayDesc.setText(weathe.getDescription());
            todayHumidity.setText( getResources().getString(R.string.weather_humidity_string) +  weathe.getHumidity().toString() +  getResources().getString(R.string.weather_humidity_unit));
            todayWind.setText( getResources().getString(R.string.weather_wind_string) +  weathe.getWind().toString() +  getResources().getString(R.string.weather_wind_unit));
            todayPressure.setText( getResources().getString(R.string.weather_pressure_string) +  weathe.getPressure().toString() +  getResources().getString(R.string.weather_pressure_unit));
            todayTempMin.setText( getResources().getString(R.string.weather_min_string) +  weathe.getTempMin().toString());
            todayTempMax.setText( getResources().getString(R.string.weather_max_string) +  weathe.getTempMax().toString());
            Picasso.with(getContext()).load(Constant.APILinks.WEATHER_ICON_URL.replace("icon",weathe.getIcon())).into(todayIcon);
            fillFiveDaysWeather(weather.getWeather());
        }catch (Exception ex){
            ex.getMessage();
        }
    }

    public  void fillFiveDaysWeather(ArrayList<Weather> weatherDays){
        try {
            fiveDaysWeather = new ArrayList<>();
            for (int i = 0; i < weatherDays.size(); i++) {
                String dateS = weatherDays.get(i).getDate();
                if(dateS.contains(weatherDays.get(0).getDate().substring(11,19))){
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = (Date) formatter.parse(dateS.substring(0,10));
                    SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
                    String dateName = outFormat.format(date);
                    Weather wea = weatherDays.get(i);
                    wea.setDayName(dateName);
                    fiveDaysWeather.add(wea);
                }
            }
            fillHorizontalWeatherList(fiveDaysWeather);
        }catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void fillHorizontalWeatherList(ArrayList<Weather> weatherList) {

        adp = new WeatherAdaptor(weatherList, getContext());
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        horizontalRecyclerView.setLayoutManager(horizontalLayoutManager);
        horizontalRecyclerView.setAdapter(adp);
        adp.setOnItemClickListener(this);
    }


    @Override
    public void onRecyclerViewItemClicked(int position, int id) {
    }

    public void loadCity(){

        try{
            cityData = new ArrayList<>();
            String cities = Constant.CityJsonData.CITYJSONDATA;
            JSONArray cityArray = new JSONArray(cities);

            for (int i = 0 ; i < cityArray.length() ; i++)
            {
                JSONObject coordinates = cityArray.getJSONObject(i).getJSONObject("coord");
                Double longitude = coordinates.getDouble("lon");
                Double latitude = coordinates.getDouble("lat");
                int id = cityArray.getJSONObject(i).getInt("id");
                String name = cityArray.getJSONObject(i).getString("name");
                String country = cityArray.getJSONObject(i).getString("country");
                City city = new City(id, name, country, longitude, latitude);
                cityData.add(city);
            }
        }catch (JSONException ex) {
            ex.getMessage();
        }
    }


    public void loadCitySpinner(){

        try{
            showProgressBar();
            loadCity();
            ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, cityData);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner.setAdapter(adapter);
            spinner.setTitle(getResources().getString(R.string.Spinner_Title));
            spinner.setPositiveButton(getResources().getString(R.string.Spinner_button));
            City c =sp.readCity();
            if(c != null){
                spinner.setSelection(c.getSpinnerPosition());
                selectCityId = c.getId();
            }else {
                selectCityId = ((City)spinner.getItemAtPosition(0)).getId();
            }

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    City city = (City) spinner.getSelectedItem();
                    city.setSpinnerPosition(i);
                    sp.writeCity(city);
                    selectCityId = city.getId();
                    if(isOffline){
                        getOfflineDataWeather(selectCityId);
                    }else {
                        getWeatherData();
                    }

                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
            hideProgressBar();

        }catch (Exception e){
            e.getMessage();
        }

    }

    public void saveDataInDatabase(WeatherList weatherList){
        db.deleteWeather(weatherList.getCity().getId());
        db.insertWeather(weatherList);
    }

    public WeatherList getOfflineDataWeather(int countryCode){
        showProgressBar();
        WeatherList list = db.selectWeather(countryCode);
        if(list.getWeather().size() == 0){
            Toast.makeText(getContext(), R.string.toast_not_available_data, Toast.LENGTH_SHORT).show();
            emptyFields();
        }else {
            fillWeatherData(list);
        }
        hideProgressBar();
        return list;
    }

    public void emptyFields(){
        todayTemp.setText("" + getResources().getString(R.string.pref_units_string));
        todayDesc.setText("");
        todayHumidity.setText( getResources().getString(R.string.weather_humidity_string) + "" +  getResources().getString(R.string.weather_humidity_unit));
        todayWind.setText( getResources().getString(R.string.weather_wind_string) +  "" +  getResources().getString(R.string.weather_wind_unit));
        todayPressure.setText( getResources().getString(R.string.weather_pressure_string) +  "" +  getResources().getString(R.string.weather_pressure_unit));
        todayTempMin.setText( getResources().getString(R.string.weather_min_string) +  "");
        todayTempMax.setText( getResources().getString(R.string.weather_max_string) +  "");
        fillFiveDaysWeather(new ArrayList<Weather>());
    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.bringToFront();
    }

    public void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void networkAvailable() {
        deviceOnline();
    }

    @Override
    public void networkUnavailable() {
        deviceOffline();
    }

    private void deviceOnline() {
        Toast.makeText(getContext(), "Online Mode", Toast.LENGTH_SHORT).show();
        isOffline = false;
        getWeatherData();
    }

    public void notifyCurrentStateToAllListeners() {
        receiver.notifyCurrentStateToAllListeners();
    }

    private void deviceOffline() {
        Toast.makeText(getContext(), "offlinneeee", Toast.LENGTH_SHORT).show();
        isOffline = true;
        getOfflineDataWeather(selectCityId);
    }


    @Override
    public void onResume() {
        super.onResume();
        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_UP && i == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    getActivity().finish();
                    return true;
                } else {

                }
                return false;
            }
        });


    }

}
