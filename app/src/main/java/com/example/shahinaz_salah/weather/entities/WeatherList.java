package com.example.shahinaz_salah.weather.entities;


import java.util.ArrayList;

public class WeatherList {
    private String cod;
    private ArrayList<Weather> weather;
    private City city;

    public WeatherList() {
    }

    public WeatherList(String cod, ArrayList<Weather> weather, City city) {
        this.cod = cod;
        this.weather = weather;
        this.city = city;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
