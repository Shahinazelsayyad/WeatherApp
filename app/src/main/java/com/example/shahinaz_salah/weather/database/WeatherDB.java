package com.example.shahinaz_salah.weather.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.example.shahinaz_salah.weather.entities.City;
import com.example.shahinaz_salah.weather.entities.Weather;
import com.example.shahinaz_salah.weather.entities.WeatherList;

import java.util.ArrayList;

public class WeatherDB extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1 ;
    public static final String DATABASE_NAME = "Weather.db";
    private static final String ENCODING_SETTING = "PRAGMA encoding ='windows-1256'";
    Context context;

    public WeatherDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        if (!db.isReadOnly()) {
            db.execSQL(ENCODING_SETTING);
        }
    }

    public void onCreate(SQLiteDatabase db) {
        try{

            final String SQL_CREATE_ENTRIES_FAV =
                    "CREATE TABLE " + WeatherTables.WeatherTable.TABLE_NAME + " (" +
                            WeatherTables.WeatherTable.COLUMN_NAME_ID + " INTEGER PRIMARY KEY, " +
                            WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY + " TEXT,  " +
                            WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY_ID + " INTEGER, " +
                            WeatherTables.WeatherTable.COLUMN_NAME_TEMP + " DOUBLE, " +
                            WeatherTables.WeatherTable.COLUMN_NAME_TEMPMIN + " DOUBLE, "  +
                            WeatherTables.WeatherTable.COLUMN_NAME_TEMPMAX + " DOUBLE, "  +
                            WeatherTables.WeatherTable.COLUMN_NAME_HUMIDITY + " DOUBLE, "  +
                            WeatherTables.WeatherTable.COLUMN_NAME_DESC + " TEXT, "  +
                            WeatherTables.WeatherTable.COLUMN_NAME_WIND + " DOUBLE, "  +
                            WeatherTables.WeatherTable.COLUMN_NAME_ICON + " TEXT, "  +
                            WeatherTables.WeatherTable.COLUMN_NAME_DATE + " TEXT, "  +
                            WeatherTables.WeatherTable.COLUMN_NAME_PRESUURE + " DOUBLE)";

            db.execSQL(SQL_CREATE_ENTRIES_FAV);

        }catch (Exception ex){
            Log.e("create database error", ex.getMessage());
        }


    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        final String SQL_CREATE_ENTRIES_FAV =
                "DROP TABLE IF EXISTS " + WeatherTables.WeatherTable.TABLE_NAME ;
        db.execSQL(SQL_CREATE_ENTRIES_FAV);

        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long insertWeather(WeatherList weather){
        long newRowId = -1;
        try{
            // Gets the data repository in write mode
            SQLiteDatabase db = this.getWritableDatabase();
            ArrayList<Weather> weathe = weather.getWeather();
            ContentValues values;
            for(int i = 0 ; i < weathe.size(); i++){
                // Create a new map of values, where column names are the keys
                values = new ContentValues();
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY, weather.getCity().getName());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY_ID, weather.getCity().getId());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_PRESUURE, weathe.get(i).getPressure());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_TEMP, weathe.get(i).getTemp());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_TEMPMAX, weathe.get(i).getTempMax());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_TEMPMIN, weathe.get(i).getTempMin());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_DESC, weathe.get(i).getDescription());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_DATE, weathe.get(i).getDate());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_HUMIDITY, weathe.get(i).getHumidity());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_ICON, weathe.get(i).getIcon());
                values.put(WeatherTables.WeatherTable.COLUMN_NAME_WIND, weathe.get(i).getWind());

                newRowId = db.insert(WeatherTables.WeatherTable.TABLE_NAME, null, values);
            }

        } catch (Exception ex){
            Log.e("insert weather", ex.getMessage());
        }
        return newRowId;
    }

    public WeatherList selectWeather(int countryID){

        ArrayList<Weather> weatherArray = null;
        WeatherList weatherList = null;
        try{
            // Gets the data repository in write mode
            SQLiteDatabase db = this.getReadableDatabase();
            String[] projection = {
                    WeatherTables.WeatherTable.COLUMN_NAME_ID,
                    WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY,
                    WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY_ID,
                    WeatherTables.WeatherTable.COLUMN_NAME_PRESUURE,
                    WeatherTables.WeatherTable.COLUMN_NAME_TEMP,
                    WeatherTables.WeatherTable.COLUMN_NAME_TEMPMAX,
                    WeatherTables.WeatherTable.COLUMN_NAME_TEMPMIN,
                    WeatherTables.WeatherTable.COLUMN_NAME_DATE,
                    WeatherTables.WeatherTable.COLUMN_NAME_ICON,
                    WeatherTables.WeatherTable.COLUMN_NAME_WIND,
                    WeatherTables.WeatherTable.COLUMN_NAME_HUMIDITY,
                    WeatherTables.WeatherTable.COLUMN_NAME_DESC
            };

            String selection = WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY_ID + " = ?";

            String[] selectionArgs = {String.valueOf(countryID)};

            // How you want the results sorted in the resulting Cursor
            String sortOrder =
                    WeatherTables.WeatherTable.COLUMN_NAME_ID + " ASC";

            Cursor cursor = db.query(
                    WeatherTables.WeatherTable.TABLE_NAME,                     // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );

            weatherArray = new ArrayList<>();
            City city = new City();
            while(cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_ID));
                String countryname = cursor.getString(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY));
                int countrID = cursor.getInt(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY_ID));
                Double pressure = cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_PRESUURE));
                Double temp = cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_TEMP));
                Double max = cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_TEMPMAX));
                Double min = cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_TEMPMIN));
                String date = cursor.getString(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_DATE));
                String desc = cursor.getString(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_DESC));
                Double humidity = cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_HUMIDITY));
                String icon = cursor.getString(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_ICON));
                Double wind = cursor.getDouble(cursor.getColumnIndexOrThrow(WeatherTables.WeatherTable.COLUMN_NAME_WIND));

                city.setId(countrID);city.setName(countryname);
               // Bitmap pic = ByteTobitmap(img);
                Weather weather = new Weather(id, temp, min, max, pressure, 0.0, 0.0, humidity, desc, icon, date, wind);
                weatherArray.add(weather);
            }
            cursor.close();
            weatherList = new WeatherList("", weatherArray, city);
            return weatherList;
        }catch (Exception ex){
            Log.e("select weather", ex.getMessage());
        }
       return weatherList;
    }

    public void deleteWeather(int countryCode){

        SQLiteDatabase db = this.getWritableDatabase();

        // Define 'where' part of query.
        String selection =  WeatherTables.WeatherTable.COLUMN_NAME_COUNTRY_ID + " = ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { String.valueOf(countryCode) };
        // Issue SQL statement.
        db.delete(WeatherTables.WeatherTable.TABLE_NAME, selection, selectionArgs);

    }


}


