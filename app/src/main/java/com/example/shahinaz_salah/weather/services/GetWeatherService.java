package com.example.shahinaz_salah.weather.services;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.shahinaz_salah.weather.entities.City;
import com.example.shahinaz_salah.weather.entities.Weather;
import com.example.shahinaz_salah.weather.entities.WeatherList;
import com.example.shahinaz_salah.weather.utility.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class GetWeatherService extends AsyncTask<String , Void , String>
implements ClientRespomseIF {

    private Context context;
    ClientRespomseIF delegate;
    private WeatherList weatherData;
    private int cityID;

    public GetWeatherService(Context context,int cityID, ClientRespomseIF delegate) {
        this.context = context;
        this.delegate = delegate;
        this.cityID = cityID;
    }

    @Override
    public void OnResponseReceived(Object result) {
    }

    @Override
    protected String doInBackground(String... params) {

        String link = Constant.APILinks.WEATHER_API_LINK;
        link = link.replace("Cityid", String.valueOf(cityID));
        String Result="";

        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();

            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
            StringBuffer buffer = new StringBuffer();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }
            Result = buffer.toString();

        } catch (java.io.IOException e) {
            e.printStackTrace();
        } catch (Exception ex){
            ex.getMessage();
        }

        getWeather(Result);
        return Result;
    }

    public String getWeather(String result)
    {
        ArrayList<Weather> weatherList = new ArrayList<>();

        try{
            JSONObject weatherJsonObj = new JSONObject(result);
            String cod = weatherJsonObj.getString("cod");

            JSONObject cityObj = weatherJsonObj.getJSONObject("city");
            int cityID = cityObj.getInt("id");
            String name = cityObj.getString("name");
            String country = cityObj.getString("country");
            Double latitude = cityObj.getJSONObject("coord").getDouble("lat");
            Double longitude = cityObj.getJSONObject("coord").getDouble("lon");
            City city = new City(cityID, name, country, longitude, latitude);

            JSONArray weatherArr = weatherJsonObj.getJSONArray("list");

            for (int i = 0 ; i < weatherArr.length() ; i++)
            {
                JSONObject mainObj = weatherArr.getJSONObject(i).getJSONObject("main");
                JSONArray weatherObj = weatherArr.getJSONObject(i).getJSONArray("weather");

                Double temp = mainObj.getDouble("temp");
                Double temp_min = mainObj.getDouble("temp_min");
                Double temp_max = mainObj.getDouble("temp_max");
                Double pressure = mainObj.getDouble("pressure");
                Double sea_level = mainObj.getDouble("sea_level");
                Double grnd_level = mainObj.getDouble("grnd_level");
                Double humidity = mainObj.getDouble("humidity");

                int id = weatherObj.getJSONObject(0).getInt("id");
                String desciption = weatherObj.getJSONObject(0).getString("description");
                String icon = weatherObj.getJSONObject(0).getString("icon");

                String date= weatherArr.getJSONObject(i).getString("dt_txt");

                JSONObject windObj = weatherArr.getJSONObject(i).getJSONObject("wind");
                Double speed = windObj.getDouble("speed");

                Weather weather = new Weather(id, temp, temp_min, temp_max, pressure, sea_level, grnd_level, humidity, desciption, icon, date, speed);
                weatherList.add(weather);
            }

            weatherData = new WeatherList(cod, weatherList, city);

        }catch (JSONException ex) {
            ex.getMessage();
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        delegate.OnResponseReceived(weatherData);
    }
}
