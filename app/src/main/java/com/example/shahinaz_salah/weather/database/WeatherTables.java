package com.example.shahinaz_salah.weather.database;

import android.provider.BaseColumns;

public final class WeatherTables {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private WeatherTables() {}

    /* Inner class that defines the table contents */
    public static class WeatherTable implements BaseColumns {
        public static final String TABLE_NAME = "Weather";
        public static final String COLUMN_NAME_ID = "weatherID";
        public static final String COLUMN_NAME_COUNTRY= "Countr_name";
        public static final String COLUMN_NAME_COUNTRY_ID = "country_id";
        public static final String COLUMN_NAME_TEMP =  "temp";
        public static final String COLUMN_NAME_TEMPMIN = "temp_min";
        public static final String COLUMN_NAME_TEMPMAX= "temp_max";
        public static final String COLUMN_NAME_PRESUURE= "pressure";
        public static final String COLUMN_NAME_WIND= "wind";
        public static final String COLUMN_NAME_DESC= "desc";
        public static final String COLUMN_NAME_HUMIDITY= "humidity";
        public static final String COLUMN_NAME_ICON= "icon";
        public static final String COLUMN_NAME_DATE= "date";
    }

}
