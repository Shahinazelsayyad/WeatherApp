package com.example.shahinaz_salah.weather.adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shahinaz_salah.weather.MainActivity;
import com.example.shahinaz_salah.weather.R;
import com.example.shahinaz_salah.weather.entities.Weather;
import com.example.shahinaz_salah.weather.utility.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class WeatherAdaptor extends  RecyclerView.Adapter<WeatherAdaptor.MyViewHolder>  {

    Context context;
    OnRecyclerViewItemClickListener listener;
    ArrayList<Weather> weather;

    public WeatherAdaptor(ArrayList<Weather> weather, Context context) {
        this.weather = weather;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView dayName;
        TextView min;
        TextView max;
        ImageView icon;

        public MyViewHolder(View view) {
            super(view);
            dayName=(TextView) view.findViewById(R.id.day_name);
            min=(TextView) view.findViewById(R.id.weather_min);
            max=(TextView) view.findViewById(R.id.weather_max);
            icon=(ImageView) view.findViewById(R.id.weather_icon);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_single_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.dayName.setText(weather.get(position).getDayName().substring(0,3));
        holder.min.setText(weather.get(position).getTempMin().toString());
        holder.max.setText(weather.get(position).getTempMax().toString());
        Picasso.with(context).load(Constant.APILinks.WEATHER_ICON_URL.replace("icon",weather.get(position).getIcon())).into(holder.icon);

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //listener.onRecyclerViewItemClicked();
            }

        });

    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public int getItemCount()
    {
        return weather.size();
    }

}
