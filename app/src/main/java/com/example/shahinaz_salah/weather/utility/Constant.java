package com.example.shahinaz_salah.weather.utility;

/**
 * Created by Chahinaz on 07/07/2017.
 */

public class Constant {

    private static final String APPID = "0291142de313cec8e1aa607d8ea257e9";
    private static final String DOMAIN_NAME = "http://api.openweathermap.org";
    private static final String TEMP_UNIT = "Metric";


    public static class APILinks
    {
        public static final String  WEATHER_API_LINK = DOMAIN_NAME + "/data/2.5/forecast?id=" + "Cityid"+ "&APPID=" + APPID + "&units=" + TEMP_UNIT;
        public static final String WEATHER_ICON_URL = DOMAIN_NAME + "/img/w/" + "icon" + ".png";
    }

    public static class BundlesConstants
    {

    }

    public static class ConnectivityStatus
    {
        public static final String WIFI ="Wifi enabled";
        public static final String MOBILE_DATA ="Mobile data enabled";
        public static final String OFFLINE ="Not connected to Internet";

    }

    public static class DataHolderKeys
    {
        public static final String CITY = "city";
    }

    public static class CityJsonData{
        public static final String CITYJSONDATA = "[\n" +
                "  {\n" +
                "    \"id\": 2917337,\n" +
                "    \"name\": \"Grimburg\",\n" +
                "    \"country\": \"DE\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 6.88333,\n" +
                "      \"lat\": 49.616669\n" +
                "    }},\n" +
                "  {\n" +
                "    \"id\": 3028808,\n" +
                "    \"name\": \"Cannes\",\n" +
                "    \"country\": \"FR\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 7.01275,\n" +
                "      \"lat\": 43.55135\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 347863,\n" +
                "    \"name\": \"Siwah\",\n" +
                "    \"country\": \"EG\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 25.51952,\n" +
                "      \"lat\": 29.20409\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 360483,\n" +
                "    \"name\": \"Muḩāfaz̧at al Wādī al Jadīd\",\n" +
                "    \"country\": \"EG\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 28.9,\n" +
                "      \"lat\": 24.799999\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 350661,\n" +
                "    \"name\": \"Qasr al Farafirah\",\n" +
                "    \"country\": \"EG\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 27.96979,\n" +
                "      \"lat\": 27.056801\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 350893,\n" +
                "    \"name\": \"Nuwaybia\",\n" +
                "    \"country\": \"EG\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 34.663399,\n" +
                "      \"lat\": 29.04681\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 352736,\n" +
                "    \"name\": \"Marsa Alam\",\n" +
                "    \"country\": \"EG\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 34.891811,\n" +
                "      \"lat\": 25.075729\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 358245,\n" +
                "    \"name\": \"Dhahab\",\n" +
                "    \"country\": \"EG\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 34.513378,\n" +
                "      \"lat\": 28.50098\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 360923,\n" +
                "    \"name\": \"Al Kharijah\",\n" +
                "    \"country\": \"EG\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 30.54635,\n" +
                "      \"lat\": 25.451401\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 361612,\n" +
                "    \"name\": \"Al Alamayn\",\n" +
                "    \"country\": \"EG\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": 28.955021,\n" +
                "      \"lat\": 30.83007\n" +
                "    }\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 5119290,\n" +
                "    \"name\": \"Green Island\",\n" +
                "    \"country\": \"US\",\n" +
                "    \"coord\": {\n" +
                "      \"lon\": -73.691513,\n" +
                "      \"lat\": 42.74424\n" +
                "    }\n" +
                "  }\n" +
                "]";
    }

}
