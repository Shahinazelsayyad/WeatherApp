package com.example.shahinaz_salah.weather.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;



public class NetworkChangeReceiver extends BroadcastReceiver {

    protected List<ConnectivityReceiverListener> listeners = new ArrayList<>();
    protected Boolean connected = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        String status = InternetConnectivity.getConnectivityStatusString(context);

       // Toast.makeText(context, status, Toast.LENGTH_LONG).show();

        switch (status){
            case "Wifi enabled":
                connected = true;
                break;
            case "Mobile data enabled":
                connected = true;
                break;
            case "Not connected to Internet":
                connected = false;
                break;
        }
        notifyStateToAll();
    }

    public void addListener(ConnectivityReceiverListener listener) {
        listeners.add(listener);
        notifyState(listener);
    }

    private void notifyStateToAll() {
        for (ConnectivityReceiverListener listener : listeners) {
            notifyState(listener);
        }
    }

    public void notifyCurrentStateToAllListeners() {
        for (ConnectivityReceiverListener listener : listeners) {
            notifyState(listener);
        }
    }

    public void removeListener(ConnectivityReceiverListener listener) {
        listeners.remove(listener);
    }

    private void notifyState(ConnectivityReceiverListener listener) {
        if (connected == null || listener == null) {
            return;
        }

        if (connected == true) {
            listener.networkAvailable();
        } else {
            listener.networkUnavailable();
        }
    }

}
