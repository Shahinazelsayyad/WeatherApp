package com.example.shahinaz_salah.weather.utility;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.example.shahinaz_salah.weather.ContactUsFragment;
import com.example.shahinaz_salah.weather.MainActivity;
import com.example.shahinaz_salah.weather.R;
import com.example.shahinaz_salah.weather.WeatherFragment;

//import com.example.chahinaz.market.MapsActivity;

/**
 * Created by Chahinaz on 14/07/2017.
 */

public  class Navigation {

    public static void  navigateToHomeActivity(Context context) {
        Fragment fragment = new WeatherFragment();
        FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_content_frame, fragment, fragment.getClass().getSimpleName());
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    public static void navigateToContactUsActivity(Context context) {
        try{
            Fragment fragment = new ContactUsFragment();
            FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.main_content_frame, fragment, fragment.getClass().getSimpleName());
            fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
            fragmentTransaction.commit();
        }catch (Exception ex){
            ex.getMessage();
        }

    }
}
