package com.example.shahinaz_salah.weather;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ContactUsFragment extends Fragment {


    @BindView(R.id.txt_name)
    EditText name;

    @BindView(R.id.txt_email)
    EditText email;

    @BindView(R.id.txt_phone)
    EditText phone;

    @BindView(R.id.txt_message)
    EditText message;

    @BindView(R.id.btn_submit)
    Button submitBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btn_submit)
    public void submitBtnHanddler(){
        if(!name.getText().toString().equals("") && !email.getText().toString().equals("") && !phone.getText().toString().equals("") && !message.getText().toString().equals("")){
            sendEmail();
        }else {
            Toast.makeText(getContext(), R.string.toast_missing_info, Toast.LENGTH_SHORT).show();
        }
    }

    protected void sendEmail() {
        String[] TO = {"eng_chahinaz@live.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "email from " + name.getText().toString());
        String emailMessage = "From: " + name.getText().toString() + " , Phone : " + phone.getText().toString() + ", email: " + email.getText().toString() +
                " , Message: " + message.getText().toString();
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailMessage);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            ex.getMessage();
        }
    }
}
