package com.example.shahinaz_salah.weather.utility;



public interface ConnectivityReceiverListener {
    void networkAvailable();

    void networkUnavailable();

}
