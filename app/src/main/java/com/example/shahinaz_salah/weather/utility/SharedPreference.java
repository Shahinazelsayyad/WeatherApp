package com.example.shahinaz_salah.weather.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.shahinaz_salah.weather.entities.City;
import com.google.gson.Gson;


/**
 * Created by shahinaz-salah on 8/1/17.
 */

public class SharedPreference {

    SharedPreferences sharedPreferences;
    Context context;
    Gson gson;

    public SharedPreference(Context context) {
        this.context = context;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        gson = new Gson();
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

    public boolean readBoolean(String id, boolean defaultValue) {
        return sharedPreferences.getBoolean(id, defaultValue);
    }

    public void writeBoolean(String id, boolean value) {
        getEditor().putBoolean(id,value).commit();
    }

    public String readString(String variable, String defaultValue) {
        return sharedPreferences.getString(variable, defaultValue);
    }

    public void writeString(String variable, String value) {
        getEditor().putString(variable, value).commit();
    }

    public void deleteString(String key) {
        getEditor().remove(key).apply();
    }

    public int readInteger(String variable, int defaultValue) {
        return sharedPreferences.getInt(variable, defaultValue);
    }

    public void writeInteger(String key, int value) {
        getEditor().putInt(key, value).commit();
    }

    public void clearData() {
        getEditor().clear().commit();
    }

    public void writeCity(City city) {
        if (city != null) {
            SharedPreferences.Editor editor = getEditor();
            editor.putString(Constant.DataHolderKeys.CITY, gson.toJson(city));
            editor.commit();
        }
    }

    public City readCity() {
        return gson.fromJson(sharedPreferences.getString(Constant.DataHolderKeys.CITY, null), City.class);
    }

}
